package com.gopaisa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gopaisa.databinding.ActivityMainBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityMainBinding mainBinding;
    final String url = "https://www.gopaisa.com";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }
        mainBinding=DataBindingUtil.setContentView(this,R.layout.activity_main);
        initView();
    }

    public void initView(){

    //    openSplash();
        checkforInternetAndOtherErrors();
        //setWebView();
        mainBinding.error.btnTryAgain.setOnClickListener(this);

    }

  /*  public void openSplash(){


    }*/

    public void checkforInternetAndOtherErrors(){

        if (Utils.getNetworkState(this)){
            mainBinding.splash.setVisibility(View.VISIBLE);
            hitApi();
            mainBinding.error.parent.setVisibility(View.GONE);

        }else {
            mainBinding.splash.setVisibility(View.GONE);
            mainBinding.error.parent.setVisibility(View.VISIBLE);
        }
    }

    public void hitApi(){
        String url = BuildConfig.BASE_URL;
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                           JSONObject jsonObject = new JSONObject(response);
                           System.out.println("response " +response);
                           if (jsonObject.optInt("status")==1)
                               setWebView();
                           else
                               error();
                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
               error();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "android-gop");
                params.put("secret", "cgt4>589*54dp@th");
                return params;
            }
        };

        int socketTimeout = 30000;
        Volley.newRequestQueue(getApplicationContext()).add(request).setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
    boolean preventCaching = true;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";


    public void setWebView(){
        mainBinding.webview.clearCache(true);
        mainBinding.webview.clearHistory();
        mainBinding.webview.getSettings().setJavaScriptEnabled(true);
        mainBinding.webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mainBinding.webview.clearCache(true);
        mainBinding.webview.loadUrl(url,preventCaching);
        mainBinding.webview.clearCache(true);
        mainBinding.webview.getSettings().setJavaScriptEnabled(true);
        mainBinding.webview.getSettings().setSupportMultipleWindows(true);
        mainBinding.webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mainBinding.webview.getSettings().setUserAgentString(getResources().getString(R.string.app_name));
        mainBinding.webview.getSettings().setUserAgentString(USER_AGENT);
        mainBinding.webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                mainBinding.splash.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnTryAgain) {
            checkforInternetAndOtherErrors();
        }
    }

    public void error(){
        mainBinding.splash.setVisibility(View.GONE);
        mainBinding.error.title.setText(getResources().getText(R.string.noRequestsFoundTitle));
        mainBinding.error.discription.setText(getResources().getText(R.string.noRequestsFoundDiscription));
        mainBinding.error.parent.setVisibility(View.VISIBLE);
    }
}
